import igraph
import openpyxl
from igraph import *
from igraph.vendor import texttable
from optlib.kpaths.kpaths import k_shortest_paths



def read_net():
    '''
    La funcion toma el archivo con los datos de la red y devuelve un grafo con atributos.
   
    OUTPUT: *g: grafo con atributos en sus nodos y vinculos.
    '''
    file_name = "PlanillaMEMayo.xlsx"
    sheet_name1 = "Equipo"
    sheet_name2 = "Link"
    
    # Leo nodos con sus atributos
    archivo = openpyxl.load_workbook( file_name )
    hoja1 = archivo.get_sheet_by_name( sheet_name1 )
    site = []
    equipo = []
    stm1 = []
    stm4 = []
    stm16 = []
    stm64 = []
    ge = []
    dge = []
    otn = []
    for i in range(2, hoja1.max_row+1):
        site.append( hoja1.cell(row=i, column=1).value )
        equipo.append( hoja1.cell(row=i, column=2).value )
        otn.append( hoja1.cell(row=i, column=3).value )        
        ge.append( hoja1.cell(row=i, column=4).value )
        dge.append( hoja1.cell(row=i, column=5).value )

    # Leo vinculos con su atributo
    hoja2 = archivo.get_sheet_by_name( sheet_name2 )
    link = []
    link_name = []
    cap = []
    for i in range(2, hoja2.max_row+1):
        link.append( (hoja2.cell(row=i, column=1).value , hoja2.cell(row=i, column=2).value) )
        cap.append( hoja2.cell(row=i, column=5).value )
        
        if hoja2.cell(row=i, column=3).value.find("OTN_CKT") >= 0:
            link_name.append( hoja2.cell(row=i, column=3).value.split("-")[2] )
        elif hoja2.cell(row=i, column=3).value.find("SeccionDWDM") >= 0:
            link_name.append( hoja2.cell(row=i, column=3).value.split("-")[3] )
        else:
            print("Revisar columna Nombre")

    # Creo grafo
    g = Graph()
    g.add_vertices( equipo )
    g.vs["name"] = site
    g.vs["equip"] = equipo
    g.vs["1GE"] = ge
    g.vs["10GE"] = dge
    g.vs["OTN"] = otn
    g.add_edges( link )
    g.es["name"] = link_name                # Nombre del vínculo
    g.es["cap"] = cap                       # Capacidad Ociosa TOTAL
    g.es["cap_me"] = [i-2 for i in cap]     # Capacidad disponibile para ME (Se reservan 2xODU0 para BBIP, el resto para ME)

    print("Nodos: ",equipo)
    print()
    print("Nodos['name']: ", site)
    print()
    print("Nodos['equip']: ", equipo)
    print()
    print("Nodos['1GE']:", ge)
    print()
    print("Nodos['10GE']:", dge)
    print()
    print("Vinculos:", link)
    print()
    print("Vinculos['name']:", link_name)
    print()
    print("Vinculos['cap']:", cap)
    plot(g, bbox=(900,900), margin=50, vertex_color="light blue", vertex_label=g.vs["name"] )
    return g




def build_net():
    '''
    La funcion arma el grafo de red segun las variables definidas a continuacion:

    INPUT:  none
    OUTPUT:  *g1: grafo de red
    '''
    Nodos = ['AS023', 'B1076', 'BA024', 'BA038', 'BA081', 'BA095', 'BA101', 'BA123', 'BA127', 'BA140', 'BA162', 'BA172', 'BA195', 'BA197', 'BA454', 'BA980', 'C1154', 'C1900', 'C2165', 'C2470',
             'C2500', 'C2518', 'C2560', 'C2561', 'C3676', 'CB026', 'CB201', 'CB204', 'CBR04', 'CBR05', 'CBR06', 'CF129', 'CF131', 'CF223', 'CF296', 'CF389', 'CFR17', 'CH050', 'CH068', 'CH070',
             'CH201', 'CO008', 'CO023', 'CO065', 'CO414', 'CO480', 'CO514', 'CO640', 'CO804', 'CO831', 'CO895', 'CO905', 'COR17', 'COR18', 'COX35', 'CR001', 'CR004', 'CR049', 'CT200', 'CT214',
             'ER001', 'ER004', 'ER009', 'ER054', 'ER132', 'FO001', 'JU001', 'JU027', 'JU028', 'JU200', 'ME099', 'ME205', 'ME298', 'MI001', 'NQ074', 'PA012', 'PA057', 'PA096', 'PA125', 'PAR03',
             'RJ001', 'RJ019', 'RJ211', 'RN012', 'RN092', 'RN118', 'RN203', 'RN217', 'RNR08', 'S1001', 'SC003', 'SC017', 'SC055', 'SC205', 'SE001', 'SF023', 'SF032', 'SF044', 'SF075', 'SF101',
             'SF126', 'SF142', 'SF235', 'SF362', 'SF390', 'SF403', 'SF903', 'SF992', 'SJ106', 'SL001', 'SL003', 'ST122', 'ST206', 'TU001', 'TU016', 'TU017', 'TU018', 'TU500']

    Nodos_equip = ['AS023ASU38', 'B1076NDJ90', 'BA024BBL38', 'BA038CNL35', 'BA081MDP38', 'BA095JNN38', 'BA101LCL90', 'BA123OLV38', 'BA127PHJ90', 'BA140IRI90', 'BA162SAV38', 'BA172TQL38', 'BA195BGD90',
                   'BA197CHV38', 'BA454TON38', 'BA980PRG38', 'C1154FST38', 'C1900MS238', 'C2165CPN38', 'C2470OLL35_8800', 'C2500-LPT35_8800', 'C2518BTC35_8800', 'C2560TIC35_8800', 'C2561GCS35_8800',
                   'C3676AST35_8800', 'CB026PMA38', 'CB201CMD38', 'CB204TLW38', 'CBR04PMT38', 'CBR05RP190', 'CBR06RP290', 'CF129JON38', 'CF131FTM35', 'CF223TOR38', 'CF296GRY38', 'CF389EDS35_8800',
                   'CFR17TTG35_8800', 'CH050RSP90', 'CH068PLA90', 'CH070LAP90', 'CH201RE390', 'CO008CRB38', 'CO023ATG90', 'CO065RCT38', 'CO414CLT90', 'CO480ALJ90', 'CO514LFA90', 'CO640VST90',
                   'CO804VMT90', 'CO831CSQ90', 'CO895COC38', 'CO905CR338', 'COR17SRX90', 'COR18CEX90', 'COX35CPX90', 'CR001CTE38', 'CR004PLL38', 'CR049MCT38', 'CT200CTM38', 'CT214CHX90', 'ER001PRN38',
                   'ER004CUR38', 'ER009LPZ38', 'ER054CCD38', 'ER132GCH38', 'FO001FMS38', 'JU001JUY38', 'JU027LGS90', 'JU028SPJ90', 'JU200JU290', 'ME099SRF38', 'ME205GYE38', 'ME298MCO38', 'MI001POS38',
                   'NQ074ZBT30-4', 'PA012GPI90', 'PA057BRN90', 'PA096CTL90', 'PA125STR38', 'PAR03BUO90', 'RJ001RJA38', 'RJ019PTQ90', 'RJ211CMX90', 'RN012CNS90', 'RN092BRE38', 'RN118LGR38',
                   'RN203VD238', 'RN217RCO90', 'RNR08CHO90', 'S1001RCO38', 'SC003PCT38', 'SC017CLT90', 'SC055PRT38', 'SC205RGL38', 'SE001SGO38', 'SF023VNT90', 'SF032RFN38', 'SF044SFE38', 'SF075SME90',
                   'SF101GLZ90', 'SF126ETB90', 'SF142CG290', 'SF235SJN90', 'SF362CSL90', 'SF390FMT90', 'SF403LRO90', 'SF903RSR38', 'SF992STN90', 'RJ001RJA38', 'SL001SLS38', 'SL003VLM38', 'ST122PCH38',
                   'ST206STA38', 'TU001TCM38', 'TU016BAR90', 'TU017CTU90', 'TU018MNT90', 'TU500MRX90']

    Nodos_1GE = [8, 0, 8, 12, 17, 18, 16, 0, 0, 24, 24, 24, 8, 38, 20, 20, 14, 0, 18, 48, 31, 16, 16, 16, 16, 15, 45, 22, 23, 0, 8, 6, 16, 30, 12, 16, 16, 12, 28, 28, 12, 25, 8, 22, 14, 14, 8, 8, 14,
                 8, 22, 7, 8, 8, 8, 30, 35, 7, 28, 8, 28, 20, 7, 15, 25, 6, 6, 16, 16, 17, 4, 20, 22, 20, 14, 12, 24, 27, 6, 8, 12, 14, 8, 8, 4, 10, 20, 0, 14, 28, 25, 6, 0, 22, 20, 12, 24, 18, 18, 16,
                 16, 17, 16, 12, 12, 16, 12, 16, 8, 20, 28, 10, 10, 25, 16, 20, 16, 16]

    Nodos_10GE = [1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 1, 0, 2, 0, 0, 2, 1, 4, 2, 0, 0, 0, 0, 0, 6, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 2, 2, 1, 0, 0,
                  0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 4, 0, 2, 2, 0, 0, 0, 0]

    Vinculos = [('AS023', 'MI001'), ('AS023', 'MI001'), ('AS023', 'MI001'), ('B1076', 'BA195'), ('PA125', 'BA024'), ('PA125', 'BA024'), ('BA024', 'RN203'), ('BA024', 'RN203'), ('BA024', 'RN203'),
                ('BA024', 'RN203'), ('BA024', 'BA123'), ('BA024', 'NQ074'), ('BA024', 'NQ074'), ('BA024', 'CBR04'), ('BA024', 'RN203'), ('BA024', 'RN203'), ('BA024', 'NQ074'), ('BA024', 'BA140'),
                ('BA024', 'RN217'), ('BA038', 'BA162'), ('BA081', 'BA454'), ('BA081', 'BA024'), ('BA095', 'BA197'), ('BA095', 'BA980'), ('BA095', 'BA980'), ('BA095', 'BA980'), ('BA095', 'BA980'),
                ('BA095', 'BA101'), ('BA101', 'SF032'), ('BA127', 'B1076'), ('BA140', 'PA057'), ('C2165', 'BA162'), ('C2165', 'BA162'), ('BA162', 'C1900'), ('BA162', 'C1900'), ('BA172', 'BA127'),
                ('BA172', 'PA096'), ('BA197', 'BA172'), ('BA197', 'BA172'), ('BA197', 'BA162'), ('BA197', 'BA195'), ('BA980', 'C1900'), ('BA980', 'C1900'), ('BA980', 'C1900'), ('C1900', 'BA123'),
                ('C1900', 'C1154'), ('C1900', 'C1154'), ('C1900', 'CF223'), ('C1900', 'CF223'), ('C1900', 'CF223'), ('C1900', 'CF223'), ('C1900', 'CF129'), ('C1900', 'CF129'), ('C2165', 'CF223'),
                ('CF223', 'C2165'), ('C2470', 'CF296'), ('C2470', 'CFR17'), ('C2500', 'C2560'), ('C2518', 'C2500'), ('C2560', 'CF296'), ('C2561', 'CF296'), ('C3676', 'CF389'), ('RN203', 'CB026'),
                ('RN203', 'CB026'), ('CB026', 'CB204'), ('CB026', 'CB204'), ('RN118', 'CB026'), ('RN118', 'CB026'), ('RN118', 'CB026'), ('RN118', 'CB026'), ('CB026', 'CB204'), ('CB026', 'CB204'),
                ('CB026', 'CB204'), ('CB026', 'CB204'), ('SC003', 'CB201'), ('CB204', 'CB201'), ('CB204', 'CB201'), ('CB204', 'CB201'), ('CB204', 'CB201'), ('CB204', 'CB201'), ('CB204', 'CB201'),
                ('CBR04', 'CB026'), ('CBR04', 'CB026'), ('CBR04', 'SC003'), ('CBR04', 'RN012'), ('CBR05', 'CBR06'), ('CBR05', 'SC003'), ('CBR06', 'CBR04'), ('C1154', 'CF129'), ('C1154', 'CF129'),
                ('CF131', 'BA038'), ('CF131', 'C2165'), ('C1154', 'CF223'), ('C1154', 'CF223'), ('CF296', 'C2470'), ('CF296', 'C2518'), ('CF296', 'CFR17'), ('CF129', 'CF296'), ('CF129', 'CF296'),
                ('CF296', 'CF223'), ('CF296', 'CF223'), ('CF296', 'CF223'), ('CF296', 'CF223'), ('CF389', 'C2470'), ('CFR17', 'C2561'), ('CFR17', 'C3676'), ('CH068', 'CH050'), ('CH070', 'CH068'),
                ('CH201', 'CH070'), ('CH201', 'CR001'), ('CO008', 'CO065'), ('CO008', 'CO065'), ('CO008', 'SF903'), ('CO008', 'S1001'), ('CO008', 'S1001'), ('CO008', 'SF044'), ('CO023', 'COX35'),
                ('CO065', 'SL003'), ('CO065', 'SL003'), ('CO065', 'SF032'), ('CO065', 'SL003'), ('CO065', 'SL003'), ('CO065', 'SL003'), ('CO065', 'CO480'), ('CO414', 'CO804'), ('CO480', 'CO414'),
                ('CO514', 'COR18'), ('CO640', 'COR17'), ('CO804', 'SF032'), ('CO831', 'CO514'), ('CO895', 'ME298'), ('CO895', 'ME298'), ('CO895', 'SF044'), ('CO895', 'CO008'), ('CO895', 'SF044'),
                ('CO895', 'CO905'), ('CO895', 'CO905'), ('CO895', 'CO905'), ('CO895', 'CO905'), ('CO895', 'CO023'), ('CO905', 'CO008'), ('CO905', 'CO008'), ('CO905', 'CO008'), ('CO905', 'CO905'),
                ('CO905', 'CO905'), ('CO905', 'CO905'), ('CO905', 'CO905'), ('CO905', 'CO008'), ('CO905', 'CO008'), ('CO905', 'CO008'), ('CO008', 'CO905'), ('CO905', 'CO008'), ('CO905', 'CO008'),
                ('COR17', 'RJ211'), ('COR18', 'CO640'), ('COX35', 'CO831'), ('ER009', 'CR001'), ('FO001', 'CR001'), ('CR001', 'FO001'), ('SF044', 'CR001'), ('CR049', 'CR004'), ('CR004', 'ER054'),
                ('ER009', 'CR049'), ('ER009', 'CR049'), ('CT200', 'RJ001'), ('CT200', 'RJ001'), ('CT200', 'RJ001'), ('CT200', 'TU500'), ('CT214', 'CT200'), ('ER009', 'ER001'), ('CR049', 'ER004'),
                ('CR049', 'ER004'), ('ER004', 'ER132'), ('ER004', 'ER132'), ('ER009', 'SF044'), ('ER054', 'CR049'), ('ER054', 'CR049'), ('ER054', 'ER004'), ('ER054', 'ER004'), ('ER054', 'ER004'),
                ('ER132', 'C2165'), ('ER132', 'C2165'), ('FO001', 'AS023'), ('JU001', 'ST206'), ('JU027', 'ST122'), ('JU028', 'JU027'), ('JU200', 'JU028'), ('JU200', 'ST206'), ('ME205', 'ME099'),
                ('ME205', 'ME099'), ('ME205', 'ME099'), ('ME298', 'ME205'), ('ME205', 'ME298'), ('ME205', 'ME298'), ('ME298', 'ME205'), ('ME205', 'ME298'), ('ME205', 'ME298'), ('CR001', 'MI001'),
                ('CR001', 'MI001'), ('MI001', 'CR004'), ('MI001', 'CR004'), ('MI001', 'AS023'), ('PA057', 'PAR03'), ('PA096', 'PA012'), ('PA096', 'PA125'), ('BA172', 'PA125'), ('BA172', 'PA125'),
                ('ME099', 'PA125'), ('PAR03', 'PA125'), ('RJ001', 'CO895'), ('RJ001', 'CO895'), ('SJ106', 'RJ001'), ('RJ001', 'CO895'), ('RJ001', 'CT214'), ('RJ019', 'RJ001'), ('RJ211', 'RJ019'),
                ('RN012', 'RN217'), ('ME099', 'RN092'), ('RN092', 'SC055'), ('NQ074', 'RN092'), ('RN203', 'RN118'), ('RN203', 'RN118'), ('RN203', 'RN118'), ('RN203', 'RN118'), ('RN217', 'RNR08'),
                ('RNR08', 'NQ074'), ('SC003', 'SC205'), ('SC003', 'SC205'), ('SC003', 'SC055'), ('SC003', 'SC017'), ('SC017', 'CB201'), ('SC205', 'SC055'), ('CB201', 'SC205'), ('SC205', 'CB201'),
                ('SE001', 'CO008'), ('SE001', 'CO008'), ('SE001', 'CO008'), ('SF023', 'SF032'), ('SF032', 'BA095'), ('SF032', 'BA095'), ('SF044', 'ER001'), ('SF075', 'SF101'), ('SF101', 'SF044'),
                ('SF126', 'SF235'), ('SF235', 'SF992'), ('SF362', 'SF390'), ('SF390', 'SF023'), ('SF403', 'SF126'), ('SF403', 'SF142'), ('SF903', 'ER004'), ('SF903', 'S1001'), ('SF903', 'S1001'),
                ('SF903', 'S1001'), ('SF903', 'BA980'), ('SF903', 'BA980'), ('SF903', 'BA980'), ('SF903', 'SF044'), ('SF903', 'SF044'), ('SF903', 'C2165'), ('C2165', 'SF903'), ('SF903', 'CF223'),
                ('SF903', 'S1001'), ('SF903', 'BA095'), ('SF044', 'SF903'), ('SF903', 'SF044'), ('SF032', 'SF903'), ('SF903', 'S1001'), ('SF903', 'S1001'), ('SF903', 'S1001'), ('SF903', 'SF362'),
                ('SF903', 'SF142'), ('SF992', 'SF075'), ('ME205', 'SJ106'), ('ME205', 'SJ106'), ('ME205', 'SJ106'), ('ME205', 'SJ106'), ('ME205', 'SJ106'), ('ME205', 'SJ106'), ('ME205', 'SJ106'),
                ('ME205', 'SJ106'), ('ME205', 'SL001'), ('ME205', 'SL001'), ('ME205', 'SL001'), ('ME205', 'SL001'), ('ME205', 'SL001'), ('ME205', 'SL001'), ('SL001', 'SL003'), ('SL001', 'SL003'),
                ('SL003', 'SF032'), ('SL003', 'SF032'), ('SL001', 'SL003'), ('SL001', 'SL003'), ('SL001', 'SL003'), ('SL001', 'SL003'), ('SL003', 'SF032'), ('ST122', 'JU001'), ('ST206', 'ST122'),
                ('FO001', 'ST122'), ('ST122', 'FO001'), ('ST206', 'TU001'), ('ST206', 'TU001'), ('ST206', 'TU001'), ('ST206', 'TU001'), ('ST206', 'TU001'), ('TU001', 'SE001'), ('TU001', 'CO008'),
                ('TU001', 'SE001'), ('TU001', 'SE001'), ('TU001', 'CO008'), ('TU001', 'CO008'), ('TU001', 'CT200'), ('TU001', 'CT200'), ('TU001', 'CT200'), ('TU016', 'TU017'), ('TU017', 'TU018'),
                ('TU018', 'TU001'), ('TU500', 'TU016')]

    Vinculos_name = ['OTN_CKT_331', 'OTN_CKT_318', 'OTN_CKT_332', 'SeccionDWDM 63c', 'OTN_CKT_42', 'OTN_CKT_115', 'OTN_CKT_288', 'OTN_CKT_289', 'OTN_CKT_290', 'OTN_CKT_291', 'OTN_CKT_313',
                     'OTN_CKT_150', 'OTN_CKT_106', 'OTN_CKT_144a', 'OTN_CKT_44', 'OTN_CKT_172', 'OTN_CKT_149', 'SeccionDWDM 68a', 'SeccionDWDM 90', 'SeccionDWDM 57a', 'OTN_CKT_324', 'OTN_CKT_325',
                     'OTN_CKT_38', 'OTN_CKT_208', 'OTN_CKT_209', 'OTN_CKT_210', 'OTN_CKT_211', 'SeccionDWDM 53', 'SeccionDWDM 54', 'SeccionDWDM 63b', 'SeccionDWDM 68b', 'OTN_CKT_36', 'OTN_CKT_133',
                     'OTN_CKT_276', 'OTN_CKT_277', 'SeccionDWDM 63a', 'SeccionDWDM 64', 'OTN_CKT_39', 'OTN_CKT_136', 'OTN_CKT_40', 'SeccionDWDM 62', 'OTN_CKT_167', 'OTN_CKT_168', 'OTN_CKT_169',
                     'OTN_CKT_61', 'OTN_CKT_176', 'OTN_CKT_177', 'OTN_CKT_188', 'OTN_CKT_189', 'OTN_CKT_190', 'OTN_CKT_191', 'OTN_CKT_192', 'OTN_CKT_193', 'OTN_CKT_35', 'OTN_CKT_96', 'SeccionDWDM 220',
                     'SeccionDWDM 212', 'SeccionDWDM 223', 'SeccionDWDM 222', 'SeccionDWDM 224', 'SeccionDWDM 217', 'SeccionDWDM 214', 'OTN_CKT_47a', 'OTN_CKT_173', 'OTN_CKT_47b', 'OTN_CKT_174',
                     'OTN_CKT_292', 'OTN_CKT_293', 'OTN_CKT_294', 'OTN_CKT_295', 'OTN_CKT_296', 'OTN_CKT_297', 'OTN_CKT_298', 'OTN_CKT_299', 'OTN_CKT_101', 'OTN_CKT_48', 'OTN_CKT_175', 'OTN_CKT_300',
                     'OTN_CKT_301', 'OTN_CKT_302', 'OTN_CKT_303', 'OTN_CKT_155', 'OTN_CKT_156', 'OTN_CKT_100b', 'SeccionDWDM 89a', 'SeccionDWDM 104a', 'SeccionDWDM 98', 'SeccionDWDM 104b',
                     'OTN_CKT_184', 'OTN_CKT_185', 'SeccionDWDM 57b', 'SeccionDWDM 58', 'OTN_CKT_180', 'OTN_CKT_181', 'SeccionDWDM 219', 'SeccionDWDM 221', 'SeccionDWDM 218', 'OTN_CKT_196',
                     'OTN_CKT_197', 'OTN_CKT_200', 'OTN_CKT_201', 'OTN_CKT_202', 'OTN_CKT_203', 'SeccionDWDM 215', 'SeccionDWDM 216', 'SeccionDWDM 213', 'SeccionDWDM 6c', 'SeccionDWDM 6b',
                     'SeccionDWDM 6a', 'SeccionDWDM 7', 'OTN_CKT_15', 'OTN_CKT_117', 'OTN_CKT_16', 'OTN_CKT_226', 'OTN_CKT_227', 'OTN_CKT_92', 'SeccionDWDM 26b', 'OTN_CKT_17', 'OTN_CKT_119',
                     'OTN_CKT_18', 'OTN_CKT_166', 'OTN_CKT_134', 'OTN_CKT_5', 'SeccionDWDM 49a', 'SeccionDWDM 49c', 'SeccionDWDM 49b', 'SeccionDWDM 26e', 'SeccionDWDM 26g', 'SeccionDWDM 49d',
                     'SeccionDWDM 26d', 'OTN_CKT_9', 'OTN_CKT_112', 'OTN_CKT_8', 'OTN_CKT_7', 'OTN_CKT_111', 'OTN_CKT_214', 'OTN_CKT_10a', 'OTN_CKT_110a', 'OTN_CKT_215', 'SeccionDWDM 26a',
                     'OTN_CKT_10b', 'OTN_CKT_110b', 'OTN_CKT_212', 'OTN_CKT_304', 'OTN_CKT_107', 'OTN_CKT_116', 'OTN_CKT_120', 'OTN_CKT_216', 'OTN_CKT_217', 'OTN_CKT_218', 'OTN_CKT_310', 'OTN_CKT_222',
                     'OTN_CKT_223', 'SeccionDWDM 26h', 'SeccionDWDM 26f', 'SeccionDWDM 26c', 'OTN_CKT_28b', 'OTN_CKT_153a', 'OTN_CKT_236', 'OTN_CKT_127', 'OTN_CKT_31b', 'OTN_CKT_159', 'OTN_CKT_204',
                     'OTN_CKT_206', 'OTN_CKT_339', 'OTN_CKT_340', 'OTN_CKT_341', 'SeccionDWDM 27c', 'SeccionDWDM 27b', 'OTN_CKT_28a', 'OTN_CKT_66a', 'OTN_CKT_65a', 'OTN_CKT_33', 'OTN_CKT_131',
                     'OTN_CKT_67a', 'OTN_CKT_31a', 'OTN_CKT_238', 'OTN_CKT_32', 'OTN_CKT_130', 'OTN_CKT_239', 'OTN_CKT_34', 'OTN_CKT_132', 'OTN_CKT_153b', 'OTN_CKT_244', 'SeccionDWDM 1c',
                     'SeccionDWDM 1b', 'SeccionDWDM 1a', 'SeccionDWDM 21', 'OTN_CKT_246', 'OTN_CKT_247', 'OTN_CKT_248', 'OTN_CKT_11', 'OTN_CKT_305', 'OTN_CKT_306', 'OTN_CKT_12', 'OTN_CKT_308',
                     'OTN_CKT_309', 'OTN_CKT_29', 'OTN_CKT_128', 'OTN_CKT_30', 'OTN_CKT_129', 'OTN_CKT_63', 'SeccionDWDM 68c', 'SeccionDWDM 65', 'SeccionDWDM 67', 'OTN_CKT_41', 'OTN_CKT_114',
                     'OTN_CKT_249', 'SeccionDWDM 68d', 'OTN_CKT_315', 'OTN_CKT_343', 'OTN_CKT_328', 'OTN_CKT_347', 'SeccionDWDM 27a', 'SeccionDWDM 26j', 'SeccionDWDM 26i', 'SeccionDWDM 89b',
                     'OTN_CKT_250', 'OTN_CKT_152', 'OTN_CKT_151', 'OTN_CKT_348', 'OTN_CKT_349', 'OTN_CKT_350', 'OTN_CKT_351', 'SeccionDWDM 91a', 'SeccionDWDM 91b', 'OTN_CKT_50', 'OTN_CKT_142',
                     'OTN_CKT_242', 'SeccionDWDM 96a', 'SeccionDWDM 96b', 'OTN_CKT_241', 'OTN_CKT_49', 'OTN_CKT_145', 'OTN_CKT_6', 'OTN_CKT_358', 'OTN_CKT_359', 'SeccionDWDM 50a', 'OTN_CKT_21',
                     'OTN_CKT_123', 'OTN_CKT_27', 'SeccionDWDM 45g', 'SeccionDWDM 45h', 'SeccionDWDM 45d', 'SeccionDWDM 45e', 'SeccionDWDM 50c', 'SeccionDWDM 50b', 'SeccionDWDM 45c',
                     'SeccionDWDM 45b', 'OTN_CKT_69', 'OTN_CKT_232', 'OTN_CKT_234', 'OTN_CKT_235', 'OTN_CKT_160', 'OTN_CKT_161', 'OTN_CKT_162', 'OTN_CKT_22', 'OTN_CKT_79', 'OTN_CKT_24', 'OTN_CKT_75',
                     'OTN_CKT_46', 'OTN_CKT_311', 'OTN_CKT_25', 'OTN_CKT_23', 'OTN_CKT_77', 'OTN_CKT_20', 'OTN_CKT_228', 'OTN_CKT_230', 'OTN_CKT_231', 'SeccionDWDM 50d', 'SeccionDWDM 45a',
                     'SeccionDWDM 45f', 'OTN_CKT_280', 'OTN_CKT_281', 'OTN_CKT_282', 'OTN_CKT_283', 'OTN_CKT_284', 'OTN_CKT_285', 'OTN_CKT_286', 'OTN_CKT_287', 'OTN_CKT_13', 'OTN_CKT_94',
                     'OTN_CKT_272', 'OTN_CKT_273', 'OTN_CKT_274', 'OTN_CKT_275', 'OTN_CKT_14', 'OTN_CKT_164', 'OTN_CKT_19', 'OTN_CKT_121', 'OTN_CKT_268', 'OTN_CKT_269', 'OTN_CKT_270', 'OTN_CKT_271',
                     'SeccionDWDM 48', 'OTN_CKT_240b', 'OTN_CKT_245a', 'OTN_CKT_240a', 'OTN_CKT_245b', 'OTN_CKT_1', 'OTN_CKT_125', 'OTN_CKT_126', 'OTN_CKT_109', 'OTN_CKT_104', 'OTN_CKT_3',
                     'OTN_CKT_171', 'OTN_CKT_356', 'OTN_CKT_357', 'OTN_CKT_352', 'OTN_CKT_326', 'OTN_CKT_317', 'OTN_CKT_333', 'OTN_CKT_334', 'SeccionDWDM 27e', 'SeccionDWDM 27f', 'SeccionDWDM 28',
                     'SeccionDWDM 27d']

    Vinculos_cap = [5, 6, 4, 2, 0, 0, 6, 6, 6, 5, 52, 0, 6, 0, 1, 2, 6, 2, 2, 2, 2, 2, 0, 0, 1, 4, 6, 2, 2, 2, 2, 4, 4, 0, 0, 2, 2, 0, 1, 0, 2, 0, 0, 3, 54, 0, 0, 0, 4, 5, 5, 0, 0, 0, 0, 2, 2, 2,
                    2, 2, 2, 2, 0, 2, 1, 2, 6, 6, 6, 6, 6, 6, 6, 6, 1, 0, 3, 6, 6, 6, 6, 3, 5, 0, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 1, 0, 3, 5, 2, 2, 2, 2, 2, 2, 2, 3, 1, 2, 3, 5, 0, 2,
                    1, 6, 0, 6, 6, 0, 2, 2, 2, 2, 2, 2, 2, 2, 4, 2, 0, 3, 1, 3, 0, 4, 2, 3, 4, 4, 0, 5, 6, 5, 7, 6, 6, 5, 0, 4, 2, 2, 2, 3, 0, 2, 1, 2, 5, 0, 0, 0, 0, 2, 2, 2, 2, 2, 4, 2, 2, 2, 2,
                    5, 1, 4, 5, 0, 1, 0, 1, 2, 2, 2, 2, 1, 5, 6, 0, 4, 6, 5, 6, 6, 0, 5, 3, 5, 3, 2, 2, 2, 0, 0, 0, 2, 4, 0, 4, 4, 2, 2, 2, 2, 4, 2, 0, 6, 6, 6, 6, 2, 2, 3, 4, 2, 2, 2, 3, 1, 4, 0,
                    5, 6, 2, 0, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 0, 1, 3, 0, 0, 0, 0, 0, 74, 0, 2, 4, 2, 3, 6, 6, 2, 2, 2, 4, 6, 6, 7, 4, 6, 6, 6, 0, 3, 6, 0, 6, 6, 0, 5, 0, 6, 0, 6, 6, 6,
                    2, 4, 1, 2, 5, 1, 5, 6, 3, 2, 0, 1, 4, 4, 0, 0, 0, 1, 1, 2, 2, 2, 2]

    g1 = Graph()
    g1.add_vertices( Nodos )
    g1.add_edges( Vinculos )
    g1.vs['name'] = Nodos
    g1.vs['equip'] = Nodos_equip
    g1.vs['1GE'] = Nodos_1GE
    g1.vs['10GE'] = Nodos_10GE
    g1.es['name'] = Vinculos_name
    g1.es['cap'] = Vinculos_cap
    g1.es["cap_me"] = [i-2 for i in Vinculos_cap]

    #plot(g1, bbox=(900,900), margin=50, vertex_color="light blue", vertex_label=g1.vs["name"] )
    return g1




def paths_calculator(g1, source, target, tipo):
    '''
    La funcion devuelve True or Falso si el servicio entre source-target puede brindarse
    por dos caminos disjuntos (en nodos y vinculos) debido a la topologia de la red.
    
    INPUT: *g: grafo de red reducida. Output de 'del_edges'
           *source: Cell ID origen del servicio
           *target: Cell ID destino del servicio
           
    OUTPUT:  *vpaths: ruta/rutas en funcion de Cell IDs de los sitios.
             *epaths: ruta/rutas en funcion de los label de los arcos.
             *True/Flase: se puede o no dar el servicio por topología?
    '''
    g = g1.as_undirected()
    
    vpaths = []
    epaths = []
    # Existe ruta entre source/target?
    if str(g.shortest_paths(source, target)[0][0]) != "inf":    
            
        # Obtengo todas las rutas mas cortas (en formato de 'index' de edge):
        n = len(g.get_all_shortest_paths(g.vs.find(name=source).index, g.vs.find(name=target).index))
        kepaths = k_shortest_paths(g, g.vs.find(name=source).index, g.vs.find(name=target).index, K=n, weights=[1]*len(g.es))

        # Obtengo todas las rutas mas cortas (en formato de 'name' de edge):
        aux2 = []
        for i in range(len(kepaths)):
            aux = []
            for j in range(len(kepaths[i])):
                aux.append( g1.es[kepaths[i][j]]["name"] )
            aux2.append(aux)
        epaths = aux2

        # Obtengo todas las rutas mas cortas (en formato de 'name' de vertex)
        vpaths = []
        for i in range(len(kepaths)):
            aux = []
            aux2 = []
            for j in range(len(kepaths[i])):
                aux.append( g1.es[kepaths[i][j]].target )
                aux.append( g1.es[kepaths[i][j]].source )
                aux2.append( g1.vs[g1.es[kepaths[i][j]].target]["name"] )
                aux2.append( g1.vs[g1.es[kepaths[i][j]].source]["name"] )
            aux2 = list(set(aux2))
            vpaths.append(aux2)
            
    else:
        vpaths = []
        epaths = []
        
    print("\nShortest-vRoutes:")
    for i in range(len(vpaths)):
        print("v#{0}: {1}".format(i, vpaths[i]) )

    print("\nShortest-eRoutes:")
    for i in range(len(epaths)):
        print("e#{0}: {1}".format(i, epaths[i]) )
        
    return vpaths, epaths




def capacity_checker(g, epaths, interfaz):
    '''
    La funcion devuelve True/False segun haya capacidad disponible en la ruta calculada.

    INPUT:  *g:  grafo en donde se calculó la ruta con atributo 'cap' en sus edges.
            *epaths: rutas del servicio como lista de listas de arcos['name']
            *interfaz:  1GE (ODU0 = 1) ó 10GE (ODU0 = 8)

    OUTPUT: *epathsOK: lista de rutas con capacidad.
    '''
    g1 = g.as_undirected()
    # Conversor interfaz --> ODU0
    if interfaz == "1GE":
        odu0 = 1
    elif interfaz == "10GE":
        odu0 = 8
    else:
        print("Interfaz invalida")

    # Reviso capacidad por arco
    output = [True]*len(epaths)
    for i in range(0, len(epaths)):
        for j in range(0, len(epaths[i])):
            if g1.es.find(name=epaths[i][j])["cap_me"] < odu0:
                output[i] = False

    if True not in output:
        print("-No se encontraron rutas con capacidad suficiente para cursar el servicio.")
        epathsOK = []

    else:        
        # Almaceno las rutas con capacidad
        epathsOK = []
        for i in range(len(epaths)):
            if output[i] == True:
                epathsOK.append( epaths[i] )

        print("\nShortest-eRoutes w/Cap:")
        for i in range(len(epathsOK)):
            print("ec#{0}: {1}".format(i, epathsOK[i]) )
        
    return epathsOK




def pair_paths(paths):
    '''
    La funcion toma una lista de listas de rutas. Se agrupan de a pares de todas las formas posibles y devuelve
    una lista de pares UNICOS de listas.

    INPUT:  *paths: lista de listas de rutas
    OUTPUT:  *upairs: lista de pares unicos de listas
    '''
    pairs = []
    
    # Si solo hay una única ruta, no se puede aparear
    if len(paths) == 1:
        print("-Se encontró una única ruta. No hay posibilidades de brindar servicio protegido.")
        upairs = []
        
    # Si hay mas de una ruta, se aparean
    else:
        # Ordeno las rutas
        for i in range(len(paths)):
            paths[i].sort()
            
        # Apareo las rutas
        for i in range(len(paths)):
            for j in range(len(paths)):
                if i!=j:
                    pairs.append( [paths[i], paths[j]] )

        # Ordeno los pares de rutas
        for i in range(len(pairs)):
            pairs[i].sort()

        # Creo una nueva lista sin pares repetidos
        upairs = []
        for i in range(len(pairs)):
            if pairs[i] not in upairs:
                upairs.append( pairs[i] )

    return upairs


def disjoint_paths2( g1, pairs, source, target):
    '''
    La funcion desecha los pares de rutas con nodos/arcos en comun.

    INPUT:  *pairs:  lista con los pares de rutas
    OUTPUT:  *dj1:  lista con los pares de rutas disjuntas en arcos
    
    '''
    # Hay elementos de repetidos entre los pares?
    dj1 = []
    output = [True]*len(pairs)
    for i in range(len(pairs)):
        for j in range(len(pairs[i][0])):
            if pairs[i][0][j] in pairs[i][1]:
                output[i] = False
                break

    # Lista con pares de rutas disjuntas en edges
    for i in range(len(pairs)):
        if output[i] == True:
            dj1.append( pairs[i] )              

    # Convierto los pares de arcos disjuntos a pares de vertices
    aux3 = []
    for i in range(len(dj1)):
        aux2 = []
        for j in range(0,2):
            aux1 = []
            for k in range(len(dj1[i][j])):
                aux1.append( g1.vs[g1.es.find(name=dj1[i][j][k]).source]["name"] )
                aux1.append( g1.vs[g1.es.find(name=dj1[i][j][k]).target]["name"] )
            aux1.remove( source )       # Elimino nodo origen de la ruta
            aux1.remove( target )       # Elimino nodo destino de la ruta
            aux2.append( aux1 )         # Agrupo la ruta en una lista (ya en formato de vertices) 
        aux3.append( aux2 )             # Agrupo los pares de rutas en una lista (en formato de vertices)

    # Hay elementos de repetidos entre los pares?
    dj2 = []
    output = [True]*len(aux3)
    for i in range(len(aux3)):
        for j in range(len(aux3[i][0])):
            if aux3[i][0][j] in aux3[i][1]:
                output[i] = False
                break

    # Lista con pares de rutas disjuntas en edges Y vertices
    for i in range(len(dj1)):
        if output[i] == True:
            dj2.append( dj1[i] )    

    print("\nDisjoint Paired Shortest-eRoutes w/Cap:")
    for i in range(len(dj2)):
        print("dec#{0}: {1}".format(i, dj2[i]) )
        
    #print(dj2)
    return dj2
        




def ports_checker(g, source, target, interfaz, tipo, cant):
    '''
    La funcion devuelve True/False segun haya puertos disponibles en los equipos terminales
    INPUT:  *g. grafo de red
            *source: sitio origen del servicio
            *target: sitio destino del servicio
            *interfaz: 1GE ó 10GE
            *tipo: 1+0, 1+1, 2x(1+0)

    OUTPUT: *True/False
    '''
    # Multiplicador de puertos. Para 2x(1+0) necesito el doble de puertos
    if tipo == "2 x 1+0":
        n = 2
    else:
        n = 1
        
    output = True
    # Corroboro si terminales poseen puertos disponibles
    if  g.vs.find(name=source)[interfaz] <  n*cant  or  g.vs.find(name=target)[interfaz] < n*cant:
        output = False

    return output
    


    

def cost_calculator(epaths, interfaz, cant, tipo):
    '''
    La función calculará el costo de brindar el servicio según la siguiente ecuacion:

    INPUT:  *epaths:  ruta(s) del servicio calculado
            *interfaz: 1GE ó 10GE
            *cant: cantidad de servicios
            
    OUTPUT: *ct: costo total de el/los servicios
    '''

    # Conversor interfaz --> ODU0
    if interfaz == "1GE":
        odu0 = 1
        oms = 200
        otn = 4500
        terminal = 2250
    elif interfaz == "10GE":
        odu0 = 8
        oms = 1600
        otn = 36000
        terminal = 18000
    else:
        print("Interfaz de servicio inválida.")

    # Costo de utilizacion de vínculos
    aux = len( epaths[0] )

    # Costo Total del serv.
    if tipo == "1+0":
        ct = 2*terminal  +  (aux-1)*otn*odu0*cant  +  aux*oms*odu0*cant 
    elif tipo == "1+1":
        ct = 2*terminal  +  2*(aux-1)*otn*odu0*cant  +  2*aux*oms*odu0*cant
        #print("2*{0} + 2*({1}-1)*{2}*{3}*{4}  +  2*{1}*{5}*{3}*{4}".format(terminal,aux,otn,odu0,cant,oms))
    elif tipo == "2 x 1+0":
        ct = 2*(2*terminal  +  (aux-1)*otn*odu0*cant  +  aux*oms*odu0*cant)
    else:
        print("Tipo de servicio inválido.")

    
    print("\n-COSTO TOTAL (USD):",ct)
    return ct




def delivery_time(c, p):
    '''
    La funcion devuelve el tiempo de entrega del servicio tomando como argumentos
    la disponibilidad de puertos en los equipos terminales y la capacidad de la
    ruta mas corta.

    INPUT: *c: True/False (Hay capacidad en la ruta mas corta calculada?)
           *p: True/False (Hay puertos disponibles en los equipos terminales?)
           
    OUTPUT: *t: plazo de entrega del servicio / status
    '''
       
    if c == True and p == True:        # CAP: OK!      -   PORTS: OK!
        t = "30 días"
        s = "Solicitud de servicio generada!"
    elif c == False and p == True:     # CAP: NOOK!    -   PORTS: OK!
        t = "365 días"
        s = "Falta capacidad"
    elif c == True and p == False:     # CAP: OK!      -   PORTS: NOOK!
        t = "180 días"
        s = "Faltan puertos"
    elif c == False and p == False:    # CAP: NOOK!    -   PORTS: NOOK!
        t = "365 días"
        s = "Falta capacidad y puertos"

    print("\n-PLAZO DE ENTREGA:",t)
    return t,s






'''
MAIN
1. Leo la red
2. Calculo la ruta (1+0) o rutas (1+1 / 2x(1+0)) mas corta/s.
3. Reviso si en la ruta calculada hay capacidad.
4. Reviso si en los equipos terminales hay puertots
5. Calculo el costo del servicio
6. Calculo el plazo de entrega

source = "CO008"
target = "TU001"
tipo = "1+0"
interfaz = "1GE"
cant = 1


g1 = build_net()
vpaths, epaths = paths_calculator(g1, source, target, tipo)
capres = capacity_checker(g1, epaths, interfaz)
disjoint_paths(capres, epaths, vpaths, source, target, tipo)
pt = ports_checker(g1, source, target, interfaz, tipo, cant)
ct = cost_calculator(epaths, interfaz, cant, tipo)
t,s = delivery_time(capres, pt)

'''

def main(source, target, interfaz, tipo, cant):
    g1 = build_net()
    vpaths, epaths = paths_calculator(g1, source, target, tipo)
    capres = capacity_checker(g1, epaths, interfaz)
    disjoint_paths(capres, epaths, vpaths, source, target, tipo)
    pt = ports_checker(g1, source, target, interfaz, tipo, cant)
    ct = cost_calculator(epaths, interfaz, cant, tipo)
    t,s = delivery_time(capres, pt)
    return t,ct,s

'''
source = "CO008"
target = "SF903"
tipo = "1+0"
interfaz = "1GE"
cant = 1
'''


def main2( source, target, interfaz, tipo, cant):
    print("DATOS DE ENTRADA:\n===============")
    print("Sitio Origen:", source)
    print("Sitio Destino:", target)
    print("Tipo de Servicio:", tipo)
    print("Interfaz:", interfaz)
    print("Cantidad:", cant)

    g1 = build_net()                                                # Cargo la red
    vpaths, epaths = paths_calculator(g1, source, target, tipo)     # Obtengo TODAS las rutas mas cortas
    epaths2 = capacity_checker(g1, epaths, interfaz)                # Me quedo con las rutas mas cortas CON CAPACIDAD


    if epaths2 != []:                                               # Si SI hay rutas con capacidad...
        if tipo == "1+1" or tipo == "2 x 1+0":
            pairs = pair_paths(epaths2)                             # Pares de rutas (e)
            newroutes = disjoint_paths2( g1, pairs, source, target) # Pares de rutas DISJUNTAS EN NODO/ARCO (e)

        if tipo == "1+0":
            newroutes = epaths2
                                                                    # Si NO hay rutas con capacidad...
    elif epaths2 == []:
        newroutes = epaths
        
    pt = ports_checker(g1, source, target, interfaz, tipo, cant)    # Hay disponibilidad de puertos?
    ct = cost_calculator(newroutes, interfaz, cant, tipo)           # Realizar el calculo de costo

    if len(epaths2) > 0:
        c = True
    else:
        c = False
    t,s = delivery_time(c, pt)                                      # Realizar el tiempo de entrega

    return t, ct, s
