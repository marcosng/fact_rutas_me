import func
from func import *
import openpyxl
from openpyxl import *
import time
import os
from appJar import gui




        
def press(btn):
    if btn=="Quit":
        app.infoBox("Esta cerrando la aplicación", "chau chau adios!")
        app.stop()
    elif btn=="Clear":
        app.clearAllEntries()
        app.setStatusbar("")
        app.setFocus('cliente')
        os.system('cls')
        app.enableButton("Run")
        
    else:
        app.disableButton("Run")
        if app.getEntry('cellida')== "" or app.getEntry('cellidb')== "" or app.getEntry('cliente')=="":
            status = "Te faltó algo! Revisar que esten completos los cellID y Nombre de cliente"
        else:
            D = app.getEntry('cliente')
            A = app.getEntry('cellida')
            B = app.getEntry('cellidb')
            TS = app.getOptionBox("Tipo de servicio")
            Q = app.getOptionBox("Cantidad de servicios")
            IS = app.getOptionBox("Interfaz de servicio")
            print(IS)
            print(TS)
            
            # LLAMANDO A FUNCION!
            t,c,s = main2(A.upper(), B.upper(), IS, TS, int(Q))
            F = time.strftime("%d/%m/%Y")
            H = time.strftime("%I:%M:%S")
            PE = t
            C = c

            # ESCRIBO RESULTADO EN EXCEL
            archivo = Workbook()
            hoja = archivo.create_sheet("Servicio",0)
            hoja['A1'] = "Fecha:"
            hoja['A3'] = "Servicio:"
            hoja['A4'] = "Cell ID A:"
            hoja['A5'] = "Cell ID B:"
            hoja['A6'] = "Tipo de Servicio:"
            hoja['A7'] = "Cantidad de servicios:"
            hoja['A8'] = "Interfaz de servio:"
            hoja['A10'] = "Plazo Estimado:"
            hoja['A11'] = "Costo USD:"
            hoja.column_dimensions["A"].width = 22
            hoja.column_dimensions["B"].width = 20

            impresion = [F+" "+H, "", D, A, B, TS, Q, IS, "", PE, C]
            for i in range(1, len(impresion)+1):
                hoja.cell(row=i, column=2).value =impresion[i-1]

            archivo.save("Servicio_"+str(D)+".xlsx")
            # ========================

 
            status = s
            app.setEntry("plazo",PE)
            app.setEntry("costo", C)
            #status = "Vamos a buscar el mejor path para tu cliente " + app.getEntry('cliente') + " entre " + app.getEntry('cellida')+" y "+app.getEntry('cellidb')
        app.setStatusbar(status)


#abro GUI 8 filas x 4 columnas
app = gui("Factibilizador ME")
app.setFont(10)
app.setStretch("both")



# Agrego titulo en la fila 0, columna0, ancho 4 columnas
app.addLabel("title", "FACTIBILIZADOR DE SERVICIO ME", 0, 0, 6)  # Row 0,Column 0,ColSpan 4
app.getLabelWidget("title").config(font=("Comic Sans", "12", "bold"))


# Agrego etiquetas y entry box para nombre de clientes, cellida y cellidb
app.setSticky("w")
app.addLabel("nombrecliente", "Cliente:", 1, 0)     # Row 1,Column 0
app.addLabel("cellida", "Cell ID A:", 2, 0)         # Row 2,Column 0
app.addLabel("cellidb", "Cell ID B:", 3, 0)         # Row 3,Column 0

app.setSticky("news")
app.addLabelOptionBox("Tipo de servicio", ["1+0","1+1","2 x 1+0"],4,0,2)
app.addLabelOptionBox("Cantidad de servicios", [1,2,3,4,5],5,0,2)
app.addLabelOptionBox("Interfaz de servicio", ["1GE", "10GE"],6,0,2)

app.setSticky("e")
app.addEntry("cliente", 1, 1)                       # Row 1,Column 1
app.addEntry("cellida", 2, 1)                       # Row 2,Column 1
app.addEntry("cellidb", 3, 1)                       # Row 3,Column 1

app.setSticky("ew")
app.setLabelAlign("nombrecliente","left")
app.setLabelAlign("cellida","left")
app.setLabelAlign("cellidb","left")
app.setLabelAlign("Tipo de servicio","left")
app.setLabelAlign("Cantidad de servicios","left")
app.setLabelAlign("Interfaz de servicio","left")



#Agrego entry box para las salidas: Plazo estimado y Costo
app.addLabel("plazo", "Plazo Estimado:", 1, 4)              # Row 2,Column 0
app.addEntry("plazo", 2, 4, 1, 1)                     # Row 2,Column 1
app.getLabelWidget("plazo").config(font=("Comic Sans", "10", "bold"))
app.addLabel("costo", "Costo U$D:", 4, 4)              # Row 2,Column 0
app.addEntry("costo", 5, 4, 1, 1)                     # Row 2,Column 1
app.getLabelWidget("costo").config(font=("Comic Sans", "10", "bold"))

#Agrego botones
#app.addButtons(["Run", "Quit"], press, 8, 0, 3) # Row 3,Column 0,Span 2
app.addButton("Run", press, 9,1,1)
app.setButtonBg("Run", "Lime")
app.setButtonPadding("Run", [20,10])
app.getButtonWidget("Run").config(font=("Comic Sans", "10", "bold"))

app.addButton("Clear", press, 9,2,1)
app.setButtonBg("Clear", "White")
app.setButtonPadding("Clear", [20,10])
app.getButtonWidget("Clear").config(font=("Comic Sans", "10", "bold"))



app.addButton("Quit", press, 9,4,1)
app.setButtonBg("Quit", "Red")
app.setButtonPadding("Quit", [20,10])
app.getButtonWidget("Quit").config(font=("Comic Sans", "10", "bold"))


#Status Bar
app.addStatusbar(fields=1, side="LEFT")
app.setStatusbarWidth(70,0)
app.setEntryFocus("cellidb")

app.go()

##cierro GUI
